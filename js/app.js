//para adicionar novas compotencias insira mais valores aqui
var data_powerups = [
	'Consumo On e Off line',
	'Modelos de Design Educacional',
	'Plataforma Amigável',
	'Escalabilidade',
	'Gestão de Ensino e Aprendizagem',
	'Ferramenta de Autoria para criação',
	'Experiência Social Learning',
	'Inteligência Artificial',
	'Custo acessível',
	'Analytics do BNCC',
	'Ensino Híbrido',
];


// lista randomizada
var random_powerups = [];

$(document).ready(function () {


	machine_powerups(1, data_powerups, 'particle1', 21000);
	machine_powerups(2, data_powerups, 'particle2', 28000);
	machine_powerups(3, data_powerups, 'particle3', 35000);




	//  Controlador do slider 
	var owl = $("#slide-vantagens");

	owl.owlCarousel({

		itemsCustom: [
			[0, 1],
			[450, 1],
			[600, 1],
			[700, 3],	
			[1000, 3],
			[1200, 3],
			[1400, 3],
			[1600, 3]
		],
		navigation: true,
		pagination: true


	});

});

//Menu ancora

$('.menu a[href^="#"]').click(function (e) {
	e.preventDefault();
	var id = $(this).attr('href'),

		targetOffset = $(id).offset().top;

	$('html, body').animate({
		scrollTop: targetOffset - 150
	}, 500);
});

$('.logo').click(function (e) {
	e.preventDefault();
	$('html, body').animate({
		scrollTop: 0
	}, 500)
});





// Botão mobile 


$('.mobile-menu').click(function () {
	$(this).toggleClass('active');
	$('.mobile-btn').toggleClass('active');
});


$('.mobile-btn').click(function () {
	$(this).toggleClass('active');
	$('.mobile-menu').toggleClass('active');
});




//rolagem menu

window.onscroll = function () {

	var rolagem = window.pageYOffset;



	//menu
	if (rolagem > 500 && $('#header').attr('class') == 'header') {

		$('#header').removeClass().addClass('header2');
		$('#particles').removeClass().addClass('particles_barra');
	}

	if (rolagem < 500 && $('#header').attr('class') == 'header2') {

		$('#header').removeClass().addClass('header');
		$('#particles').removeClass().addClass('particles');

	}

}






var current_powerups = [100, 100, 100];

function machine_powerups(slot, powerups, capsule, timer) {

	var gat_powerups = false;
	var item = powerups[Math.floor(Math.random() * powerups.length)];

	for (var i = 0; i <= current_powerups.length; i++) {
		if (i != slot) {
			if (item != current_powerups[i]) {
				gat_powerups = true;
				current_powerups[slot] = item;
			} else {
				gat_powerups = false;
				console.log('\n' + capsule + ': ' + item + ' (Reprovado >> Repetiu o item da particle' + (i + 1) + ')\n\n');

				break
			}
		}
	}

	if (gat_powerups) {
		$('#' + capsule).fadeOut(function () {
			$(this).text(item).fadeIn('slow');
		});



		console.log(capsule + ': ' + item);
		setTimeout(function () { machine_powerups(slot, powerups, capsule, timer); }, timer);
		gat_powerups = false;
	} else {
		machine_powerups(slot, powerups, capsule, timer);
	}


}


function contato() {


	if ($(window).width() >= 760) {

		$('html, body').animate({
			scrollTop: 0
		}, 500)

		$('#modal_fundo').removeClass("modal_fundo_saida");
		$('#modal').removeClass("modal_saida");

		$('#modal_fundo').addClass("modal_fundo_entrada");
		$('#modal').addClass("modal_entrada");
	} else {

		window.location.href = "https://docs.google.com/forms/d/e/1FAIpQLSe0WKf2IZIuZboHRn30i7KZivE8yRjSowMdiCy8aw1BkktLYg/viewform";

	}

}



function close_contato() {

	$('html, body').animate({
		scrollTop: 0
	}, 500)

	$('#modal_fundo').removeClass("modal_fundo_entrada");
	$('#modal').removeClass("modal_entrada");
	$('#modal_fundo').addClass("modal_fundo_saida");
	$('#modal').addClass("modal_saida");


}



function access() {
	window.location.href = "http://app.aulapp.com.br/";

}